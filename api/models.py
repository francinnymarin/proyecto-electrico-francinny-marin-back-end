#import uuid
from django.db import models


# Create your models here.

class Record(models.Model):
   fecha = models.DateField(blank=False, editable=False)
   direccion = models.TextField(blank=False, editable=True)
   categoria = models.CharField(blank=False, editable=False, max_length=50)
   frase = models.TextField(blank=False, editable=False)


   def _str_(self):
     return self.id