from django.urls import path
from .views import RecordView

urlpatterns = [
    path('records/', RecordView.as_view(), name='record-endpoint')
]
