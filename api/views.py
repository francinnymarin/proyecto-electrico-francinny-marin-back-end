from django.http import JsonResponse
from django.utils.datetime_safe import datetime
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import View
import json
import base64

from api.models import Record


class RecordView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        jsonData = json.loads(request.body)

        name = './Grabaciones/'+str(jsonData['categoria'])+'/' + str(jsonData['fecha']) + '.wav'
        wav_file = open(name, "wb")
        decode_string = base64.b64decode(jsonData['file'])
        wav_file.write(decode_string)

        toSave = Record(fecha=datetime.fromtimestamp(int(jsonData['fecha'])), direccion=name,
                        categoria=jsonData['categoria'], frase=jsonData['frase'])
        toSave.save()
        data = {'message': "Success"}
        return JsonResponse(data)
# Create your views here.
